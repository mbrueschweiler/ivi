//Applikationsvariablen
const express = require("express");
const app = express();
const PORT = process.env.PORT || 3000;

//Cross-origin fuer alle abfragen
var cors = require("cors");
app.use(cors());

//Static files
app.use(express.static("./public"));

//JSON files
var text = require("./text.json");

//Bodyparser
const bodyParser = require("body-parser");
app.use(bodyParser.json());

const fs = require("fs");

//Applikation auf Port starten
app.listen(PORT, () => {
 console.log("Server running on port 3000");
});

//Get all texts
app.get("/text", (req, res, next) => {
    res.send(text);
});